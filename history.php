<?php
session_start();
if (empty($_SESSION['login'])) {
    header("Location: login.php");
    exit;
}

$conn = mysqli_connect("localhost", "root", "", "tv_shop");

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$user_id = $_SESSION['user_id'];

$sql = "SELECT * FROM purchase_history WHERE user_id=$user_id ORDER BY stan";
$result = mysqli_query($conn, $sql);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Історія замовлень</title>
    <link href="css/history.css" rel="stylesheet">
    <link href="css/scrollbar.css" rel="stylesheet">
</head>
<body>
<header class="header">
    <h1>Історія замовлень</h1>
    <a href="index.php" class="button">На головну</a>
    <a href="cart.php" class="button">Повернутися до кошика</a>
</header>
<main>
    <section class="history">
        <?php if (mysqli_num_rows($result) > 0) { ?>
            <table class="history-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Дата</th>
                    <th>Телефон</th>
                    <th>ПІБ</th>
                    <th>Місто</th>
                </tr>
                </thead>
                <tbody>
                <?php while ($row = mysqli_fetch_assoc($result)) { ?>
                    <tr onclick="toggleProducts(<?php echo $row['id']; ?>)">
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['date']; ?></td>
                        <td><?php echo $row['phone']; ?></td>
                        <td><?php echo $row['PIB']; ?></td>
                        <td><?php echo $row['city']; ?></td>
                    </tr>
                    <tr id="productsRow_<?php echo $row['id']; ?>" style="display: none;">
                        <td colspan="5">
                            <?if($row['stan'] == 0){?>
                                <p style="color: darkorange">Виконується</p>
                            <?}?>
                            <?if($row['stan'] == 1){?>
                                <p style="color: green">Виконано</p>
                            <?}?>
                            <?if($row['stan'] == 2){?>
                                <p style="color: red">Відмінено</p>
                            <?}?>
                            <?php
                            $product_ids = explode('*', $row['product_ids']);
                            $quantities = explode('*', $row['quantities']);
                            for ($i = 0; $i < count($product_ids); $i++) {
                                $product_id = $product_ids[$i];
                                $quantity = $quantities[$i];

                                $product_sql = "SELECT * FROM products WHERE id_product=$product_id";
                                $product_result = mysqli_query($conn, $product_sql);
                                $product_row = mysqli_fetch_assoc($product_result);
                                $product_title = $product_row['name'];
                                $product_image = $product_row['image'];
                                $product_price = $product_row['price'];

                                echo "<img src=\"$product_image\" alt=\"Телевізор\" class=\"product-image\">";
                                echo "<p style='margin: 0;'>" . $product_title . "(" .$product_price . " грн.)</p>" . " (Кількість: " . $quantity . ")<br>";
                                echo "<hr>";
                            }
                            echo "<p>Сума: " . $row['suma'] . " грн.</p>";
                            ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <p>У вас немає замовлень</p>
        <?php } ?>
    </section>
</main>
<script>
    function toggleProducts(rowId) {
        var productsRow = document.getElementById("productsRow_" + rowId);
        productsRow.style.display = productsRow.style.display === "none" ? "table-row" : "none";
    }
</script>
</body>
</html>