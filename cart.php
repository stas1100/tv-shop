<?php
session_start();
if(empty($_SESSION['login'])){
    header("Location: login.php");
}

$conn = mysqli_connect("localhost", "root", "", "tv_shop");

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if (isset($_SESSION['login'])) {
    $user_id = $_SESSION['user_id'];

    if (isset($_GET['remove'])) {
        $product_id = $_GET['remove'];
        $sql = "DELETE FROM cart WHERE user_id=$user_id AND product_id=$product_id";
        mysqli_query($conn, $sql);
    }

    $sql = "SELECT products.*, cart.quantity FROM cart INNER JOIN products ON cart.product_id=products.id_product WHERE cart.user_id=$user_id";
    $result = mysqli_query($conn, $sql);


    if (isset($_POST['buy'])) {
        $selectedProducts = $_POST['selected_products'];
        $Products = implode('*', $selectedProducts);
        $quants = $_POST['selected_quantities'];
        $quantities = implode('*', $quants);

        $date = date('Y-m-d H:i:s');
        $address = $_POST['address'];
        $phone = $_POST['phone'];
        $pib = $_POST['lastName'] . ' ' . $_POST['firstName'] . ' ' . $_POST['middleName'];
        $city = $_POST['city'];
        $total = $_POST['totalP'];
        $stan = 0;
        $Sql = "INSERT INTO purchase_history (user_id, product_ids, quantities, date, address, phone, PIB, city, suma, stan) VALUES ($user_id, '$Products', '$quantities', '$date', '$address', '$phone', '$pib', '$city','$total', {$stan})";

        if(mysqli_query($conn, $Sql)) {
            foreach ($selectedProducts as $product) {
                $clearCartSql = "DELETE FROM cart WHERE user_id=$user_id AND product_id =$product";
                mysqli_query($conn, $clearCartSql);
                $_SESSION['cart']--;
            }
        }
        header("Location: cart.php");
    }
}else {
        $result = false;
    }
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Кошик</title>
        <link href="css/cart.css" rel="stylesheet">
        <link href="css/scrollbar.css" rel="stylesheet">
        <link href="css/chbNrb.css" rel="stylesheet">
    </head>
    <body>
    <header class="header">
        <h1>Кошик</h1>
        <a href="index.php" class="button">На головну</a>
    </header>
    <main>
        <a href="history.php" style="margin: 15px;" class="button hist">Історія замовлень</a>
        <section class="cart">
            <h2>Кошик</h2>
            <?php if ($result) { ?>
            <form action="cart.php" method="post">
                    <table>
                        <thead>
                        <tr>
                            <th>Назва</th>
                            <th>Ціна</th>
                            <th>Кількість</th>
                            <th>Всього</th>
                            <th> </th>
                            <th>Вибрати</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php while ($row = mysqli_fetch_assoc($result)) { ?>
                            <tr>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['price']; ?> грн.</td>
                                <td><input type="hidden" id="quants" value="<?php echo $row['quantity'];?>"><?php echo $row['quantity'];?></td>
                                <td><?php echo $row['price'] * $row['quantity']; ?> грн.</td>
                                <td><a href="functions/clear-cart.php?id_product=<?php echo $row['id_product'];?>&quantity=<?php echo $row['quantity']; ?>" class="button">Видалити</a></td>
                                <td><input type="checkbox" class="custom-checkbox" value="<?echo $row['id_product']?>" id="<?echo $row['id_product']?>">
                                    <label for="<?echo $row['id_product']?>"></label>
                                </td>
                            </tr>
                            <?
                            ?>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">Загальна сума:</td>
                            <td id="total-amount" colspan="3">0.00 грн.</td>
                        </tr>
                        <tr>
                            <td colspan="4"><a id="buy-btn" class="button">Купити</a></td>
                            <td colspan="2"><a href="functions/clear-cart.php?id_product=-1" class="button">Очистити кошик</a></td>
                        </tr>
                        </tfoot>
                    </table>
            <?php } else { ?>
                <p>Your cart is empty.</p>
            <?php } ?>
        </section>
        <div id="buy-form" class="container">
                <h1>Оформлення замовлення</h1>
            <input id="totalPrice" type="hidden" name="totalP" value="0.00">
                <label for="firstNameInput">Ім'я:</label>
                <input type="text" id="firstNameInput" name="firstName">
                <label for="lastNameInput">Прізвище:</label>
                <input type="text" id="lastNameInput" name="lastName">
                <label for="middleNameInput">По батькові:</label>
                <input type="text" id="middleNameInput" name="middleName">
                <label for="phoneInput">Номер телефону:</label>
                <input type="text" id="phoneInput" name="phone">
                <label for="cityInput">Введіть місто:</label>
                <input type="text" id="cityInput" name="city">
                <label for="warehousesSelect">Виберіть відділення:</label>
                <select name="address" id="warehousesSelect" style="max-height: 200px; overflow-y: scroll;"></select>
                <input type="submit" style="margin-top: 16px;" name="buy" value="Оформити">
        </div>
        </form>
    </main>
    <script>
        let buyBtn = document.getElementById('buy-btn');
        let buyForm = document.getElementById('buy-form');
        let totalPrice = document.getElementById('totalPrice');
        let back = document.querySelector('.cart');
        let productCheckboxes = document.querySelectorAll('.custom-checkbox');
        let quants = document.querySelectorAll('#quants');
        let totalAmount = document.querySelector('#total-amount');
        let total = 0;

        document.addEventListener('click', function (event) {
            if (event.target.closest('#buy-form') || event.target.closest('#buy-btn')) {
                return;
            }
                buyForm.style.display = 'none';
                back.style.pointerEvents = 'auto';
                back.classList.remove('blur-background');
        });

        for(let i = 0; i < productCheckboxes.length; i++) {
            productCheckboxes[i].addEventListener('change', () => {
                let row = productCheckboxes[i].parentNode.parentNode;
                let price = parseFloat(row.querySelector('td:nth-child(2)').textContent);
                let quantity = parseFloat(row.querySelector('td:nth-child(3)').textContent);
                if (productCheckboxes[i].checked) {
                    total += price * quantity;
                    productCheckboxes[i].name = "selected_products[]";
                    quants[i].name = "selected_quantities[]";
                } else {
                    total -= price * quantity;
                    productCheckboxes[i].name = "not_selected[]";
                    quants[i].name = "not_quantities[]";
                }
                totalAmount.textContent = total.toFixed(2) + " грн.";
                totalPrice.value = total.toFixed(2);
                console.log(totalPrice.value);
            });
        };

        buyBtn.addEventListener('click', () => {
            let selectedProducts = Array.from(productCheckboxes).filter(checkbox => checkbox.checked);
            if (selectedProducts.length === 0) {
                alert('Виберіть хоча б один товар перед оформленням замовлення.');
                return;
            }
            buyForm.style.display = 'flex';
            back.style.pointerEvents = 'none';
            back.classList.add('blur-background');
        });

        const apiKey = '6d7b91da576ea017cd60367b94dab111';
        const cityInput = document.getElementById('cityInput');
        const warehousesSelect = document.getElementById('warehousesSelect');

        function searchWarehouses(cityName) {
            const limit = 100;
            const url = 'https://api.novaposhta.ua/v2.0/json/';
            const data = {
                "apiKey": apiKey,
                "modelName": "Address",
                "calledMethod": "getWarehouses",
                "methodProperties": {
                    "CityName": cityName,
                    "Page": "1",
                    "Limit": limit,
                    "Language": "UA"
                }
            };
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(response => response.json())
                .then(result => {
                    if (result.success) {
                        let warehouses = result.data;
                        let html = '';
                        warehouses.forEach(function(warehouse) {
                            html += `<option value="${warehouse.Description}">${warehouse.Description}</option>`;
                        });
                        warehousesSelect.innerHTML = html;
                    } else {
                        warehousesSelect.innerHTML = '';
                        console.error('Помилка: ' + result.errors[0].code + ' - ' + result.errors[0].message);
                    }
                })
                .catch(error => console.error(error));
        }

        cityInput.addEventListener('input', function() {
            let cityName = cityInput.value.trim();
            if (cityName !== '') {
                searchWarehouses(cityName);
            } else {
                warehousesSelect.innerHTML = '';
            }
        });
    </script>
    </body>
    </html>
<?php
// Close the database connection
mysqli_close($conn);
?>