<?php
session_start();

$conn = mysqli_connect("localhost", "root", "", "tv_shop");
$serch = "";
$Size1 = "";
$Size2 = "";
$Size3 = "";
$Size4 = "";
$Size5 = "";
$PriceASC = "checked";
$PriceDESC = "";
$Man1 = "";
$Man2 = "";
$Man3 = "";
$Man4 = "";
$Man5 = "";
$Man6 = "";
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT * FROM products WHERE stock_quantity != 0 ";

function addS($str, $num){
    global $check;
    global $sql;
    if($check){
        $sql .= "AND (";
        $check = false;
    }else{
        $sql .= "OR ";
    }
    if($num == 1) {
        $AB = explode(",", $str);
        $A = $AB[0];
        $B = $AB[1];
        $sql .= "screen_size BETWEEN {$A} AND {$B} ";
    }
    if($num == 2){
        $sql .= "manufacturer LIKE '{$str}' ";
    }
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $check = true;
    if (!empty($_POST["screen_size1"])) {
        addS($_POST["screen_size1"], 1);
        $Size1 = "checked";
    }
    if (!empty($_POST["screen_size2"])) {
        addS($_POST["screen_size2"], 1);
        $Size2 = "checked";
    }
    if (!empty($_POST["screen_size3"])) {
        addS($_POST["screen_size3"], 1);
        $Size3 = "checked";
    }
    if (!empty($_POST["screen_size4"])) {
        addS($_POST["screen_size4"], 1);
        $Size4 = "checked";
    }
    if (!empty($_POST["screen_size5"])) {
        addS($_POST["screen_size5"], 1);
        $Size5 = "checked";
    }
    if(!$check){
        $sql .= ")";
    }
    $check = true;
    if (!empty($_POST["man1"])) {
        addS($_POST["man1"], 2);
        $Man1 = "checked";
    }
    if (!empty($_POST["man2"])) {
        addS($_POST["man2"], 2);
        $Man2 = "checked";
    }
    if (!empty($_POST["man3"])) {
        addS($_POST["man3"], 2);
        $Man3 = "checked";
    }
    if (!empty($_POST["man4"])) {
        addS($_POST["man4"], 2);
        $Man4 = "checked";
    }
    if (!empty($_POST["man5"])) {
        addS($_POST["man5"], 2);
        $Man5 = "checked";
    }
    if (!empty($_POST["man6"])) {
        addS($_POST["man6"], 2);
        $Man6 = "checked";
    }
    if(!$check){
        $sql .= ")";
    }
    if(!empty($_POST["search_name"])){
        $sql .= "AND (name LIKE '%{$_POST["search_name"]}%' OR description LIKE '%{$_POST["search_name"]}%' OR manufacturer LIKE '%{$_POST["search_name"]}%' OR category LIKE '%{$_POST["search_name"]}%')";
        $serch = $_POST["search_name"];
    }
    if (!empty($_POST["price"])) {
        if ($_POST["price"] == "asc") {
            $sql .= "ORDER BY price ASC ";
            $PriceASC = "checked";
        } elseif ($_POST["price"] == "desc") {
            $sql .= "ORDER BY price DESC ";
            $PriceDESC = "checked";
        }
    }
}else{
    $sql .= " ORDER BY price ASC ";
}

$result = mysqli_query($conn, $sql);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Магазин</title>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/scrollbar.css" rel="stylesheet">
    <link rel="stylesheet" href="css/chbNrb.css">
</head>
<body>
<header>
    <h1>TV Store</h1>
    <nav>
        <ul class="main-nav">
            <li><a href="index.php" class="button">Головна</a></li>
            <li><a href="shop.php" class="button">Магазин</a></li>
            <li><a href="about.php" class="button">Про проект</a></li>
        </ul>
        <ul>
            <?php if (isset($_SESSION['login']) || $_SESSION['login'] != "" ) { ?>
                <li class="dropdown">
                    <a class="button login"><? echo $_SESSION['login']?></a>
                    <ul class="dropdown-menu">
                    <?php if ($_SESSION['isAdmin']) { ?>
                        <li><a href="admin.php" class="button">Адмінпанель</a></li>
                    <?php }?>
                        <li><a href="cart.php" class="button">Кошик(<?php echo $_SESSION['cart']; ?>)</a></li>
                        <li><a href="logout.php" class="button">Вийти</a></li>
                    </ul>
                </li>
            <?php } else { ?>
                <li><a href="login.php" class="login">Увійти</a></li>
                <li><a href="register.php" class="login">Зареєструватись</a></li>
            <?php } ?>
        </ul>
    </nav>
</header>
<div id="sidebar">
    <h3>Пошук</h3>
    <form method="post" action="shop.php">
        <label>
            <input type="text" name="search_name" value="<?echo $serch?>" placeholder="Пошук">
        </label>
        <input type="submit" name="search" value="Шукати">
    <h3>Діагональ екрану</h3>
    <ul>
        <li>
            <input type="checkbox" <?echo $Size1?> class="custom-checkbox" id="screen_size1" name="screen_size1" value="20,29">
            <label for="screen_size1">20-29 дюймів</label>
        </li>
        <li>
            <input type="checkbox" <?echo $Size2?> class="custom-checkbox" id="screen_size2" name="screen_size2" value="30,39">
            <label for="screen_size2">30-39 дюймів</label>
        </li>
        <li>
            <input type="checkbox" <?echo $Size3?> class="custom-checkbox" id="screen_size3" name="screen_size3" value="40,49">
            <label for="screen_size3">40-49 дюймів</label>
        </li>
        <li>
            <input type="checkbox" <?echo $Size4?> class="custom-checkbox" id="screen_size4" name="screen_size4" value="50,59">
            <label for="screen_size4">50-59 дюймів</label>
        </li>
        <li>
            <input type="checkbox" <?echo $Size5?> class="custom-checkbox" id="screen_size5" name="screen_size5" value="60,69">
            <label for="screen_size5">60-69 дюймів</label>
        </li>
    </ul>
        <h3>Виробник</h3>
        <ul>
            <li>
                <input type="checkbox" <?echo $Man1?> class="custom-checkbox" id="man1" name="man1" value="Samsung">
                <label for="man1">Samsung</label>
            </li>
            <li>
                <input type="checkbox" <?echo $Man2?> class="custom-checkbox" id="man2" name="man2" value="LG">
                <label for="man2">LG</label>
            </li>
            <li>
                <input type="checkbox" <?echo $Man3?> class="custom-checkbox" id="man3" name="man3" value="Nokia">
                <label for="man3">Nokia</label>
            </li>
            <li>
                <input type="checkbox" <?echo $Man4?> class="custom-checkbox" id="man4" name="man4" value="Philips">
                <label for="man4">Philips</label>
            </li>
            <li>
                <input type="checkbox" <?echo $Man5?> class="custom-checkbox" id="man5" name="man5" value="Kivi">
                <label for="man5">Kivi</label>
            </li>
            <li>
                <input type="checkbox" <?echo $Man6?> class="custom-checkbox" id="man6" name="man6" value="Sony">
                <label for="man6">Sony</label>
            </li>
        </ul>
    <h3>Сортувати за ціною</h3>
    <ul>
        <li>
            <input type="radio" <?echo $PriceASC?> class="custom-radio" id="option1" name="price" value="asc">
            <label for="option1">Низька - Висока</label>
        </li>
        <li>
            <input type="radio" <?echo $PriceDESC?> class="custom-radio" id="option2" name="price" value="desc">
            <label for="option2">Висока - Низька</label>
        </li>
    </ul>
    </form>
</div>
<main>
    <section class="products">
        <h2>Товари</h2>
        <?php if ($result == false) {
            echo $sql;
        }?>
            <?php if (mysqli_num_rows($result) == 0) {
                echo "Пусто.";
            }?>
        <ul>
            <?php while($row = mysqli_fetch_assoc($result)) { ?>
                <li class="prod-block">
                    <a href="tv-info.php?id=<?php echo $row['id_product']?>">
                        <img src="<?php echo $row['image']; ?>" alt="<?php echo $row['name']; ?>">
                    </a>
                    <h3><?php echo $row['name']; ?></h3>
                    <p><?php echo $row['screen_size']; ?>"</p>
                    <p>Ціна: <?php echo $row['price']; ?> грн.</p>
                    <a href="functions/add_to_cart.php?add_to_cart=<?php echo $row['id_product']?>" class="button addBut">В корзину</a>
                </li>
            <?php } ?>
        </ul>
    </section>
</main>
<footer>
    &copy; TV Store 2023
</footer>
<script>
    console.log("<?php echo $sql?>");
    let sidebar = document.getElementById('sidebar');
    sidebar.addEventListener('mouseover', function() {
        this.classList.add('open');
    });

    sidebar.addEventListener('mouseout', function() {
        this.classList.remove('open');
    });

    let option1 = document.getElementById("option1");
    let option2 = document.getElementById("option2");

    option1.addEventListener("click", function() {
        option2.checked = false;
    });

    option2.addEventListener("click", function() {
        option1.checked = false;
    });
</script>
</body>
</html>
<?php
mysqli_close($conn);
?>