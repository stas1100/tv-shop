<?php
session_start();
if(empty($_SESSION['login'])){
    header("Location: ../login.php");
}

$conn = mysqli_connect("localhost", "root", "", "tv_shop");

$firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
$lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
$middleName = mysqli_real_escape_string($conn, $_POST['middleName']);
$phone = mysqli_real_escape_string($conn, $_POST['phone']);
$city = mysqli_real_escape_string($conn, $_POST['city']);
$warehouse = mysqli_real_escape_string($conn, $_POST['warehouse']);
$productIds = mysqli_real_escape_string($conn, $_SESSION['product_ids']);
$quantities = mysqli_real_escape_string($conn, $_SESSION['quantities']);

$cart = "SELECT * FROM cart WHERE user_id={$_SESSION['user_id']}";
$result = mysqli_query($conn, $cart);
while ($row = mysqli_fetch_assoc($result)){
    mysqli_query($conn, "UPDATE products SET stock_quantity = stock_quantity - {$row['quantity']} WHERE id_product ={$row['product_id']}");
    mysqli_query($conn, "UPDATE products SET selled_quantity = selled_quantity + {$row['quantity']} WHERE id_product ={$row['product_id']}");
}

// Insert into purchase_history
$insertQuery = "INSERT INTO purchase_history (user_id, product_ids, quantities, purchase_date, phone, city, warehouse) 
                VALUES (?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?)";
$stmt = mysqli_prepare($conn, $insertQuery);
mysqli_stmt_bind_param($stmt, "issssssss", $_SESSION['user_id'], $productIds, $quantities, $firstName, $lastName, $middleName, $phone, $city, $warehouse);
mysqli_stmt_execute($stmt);

// Clear cart
$clear = "DELETE FROM cart WHERE user_id ={$_SESSION['user_id']}";
$result = mysqli_query($conn, $clear);
$_SESSION['cart'] = 0;

mysqli_close($conn);
header("Location: " . $_SERVER['HTTP_REFERER']);
?>