<?php
session_start();
if (!isset($_SESSION['login']) || !$_SESSION['isAdmin']) {
    header('Location: login.php');
    exit;
}

$conn = mysqli_connect("localhost", "root", "", "tv_shop");

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$stan = 3;
if (isset($_GET['func'])) {
    $id = $_GET['id'];
    $stan = $_GET['func'];
    mysqli_query($conn, "UPDATE purchase_history SET stan = $stan WHERE id ={$id}");
    header('Location: orders.php');
}
if (isset($_GET['stan'])) {
    $stan = $_GET['stan'];

    $sql = "SELECT * FROM purchase_history WHERE stan='$stan'";
    $result = mysqli_query($conn, $sql);
} else {
    $stan = 3;
    $sql = "SELECT * FROM purchase_history ORDER BY date DESC";
    $result = mysqli_query($conn, $sql);
}

?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Замовлення</title>
        <link href="css/history.css" rel="stylesheet">
        <link href="css/scrollbar.css" rel="stylesheet">
    </head>
    <body>
    <header class="header">
        <h1>Замовлення</h1>
        <a href="admin.php" class="button">Панель адміністратора</a>
        <a href="index.php" class="button">На головну</a>
    </header>
    <main>
        <section class="orders">
            <div class="buttons">
                <a href="orders.php" class="button">Усі</a>
                <a href="orders.php?stan=0" class="button">Виконується</a>
                <a href="orders.php?stan=1" class="button">Виконано</a>
                <a href="orders.php?stan=2" class="button">Відмінено</a>
            </div>
            <?php if (mysqli_num_rows($result) > 0) { ?>
                <table class="history-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Дата</th>
                        <th>Телефон</th>
                        <th>ПІБ</th>
                        <th>Місто</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php while ($row = mysqli_fetch_assoc($result)) { ?>
                        <tr onclick="toggleProducts(<?php echo $row['id']; ?>)">
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['date']; ?></td>
                            <td><?php echo $row['phone']; ?></td>
                            <td><?php echo $row['PIB']; ?></td>
                            <td><?php echo $row['city']; ?></td>
                        </tr>
                        <tr id="productsRow_<?php echo $row['id']; ?>" style="display: none;">
                            <td colspan="5">
                                <?if($row['stan'] == 0){?>
                                    <p style="color: darkorange">Виконується</p>
                                <?}?>
                                <?if($row['stan'] == 1){?>
                                    <p style="color: green">Виконано</p>
                                <?}?>
                                <?if($row['stan'] == 2){?>
                                    <p style="color: red">Відмінено</p>
                                <?}?>
                                <p><?php echo $row['address']; ?></p>
                                <?php
                                $product_ids = explode('*', $row['product_ids']);
                                $quantities = explode('*', $row['quantities']);
                                for ($i = 0; $i < count($product_ids); $i++) {
                                    $product_id = $product_ids[$i];
                                    $quantity = $quantities[$i];

                                    $product_sql = "SELECT * FROM products WHERE id_product=$product_id";
                                    $product_result = mysqli_query($conn, $product_sql);
                                    $product_row = mysqli_fetch_assoc($product_result);
                                    $product_title = $product_row['name'];
                                    $product_image = $product_row['image'];
                                    $product_price = $product_row['price'];

                                    echo "<img src=\"$product_image\" alt=\"Телевізор\" class=\"product-image\">";
                                    echo "<p style='margin: 0;'>" . $product_title . "(" .$product_price . " грн.)</p>" . " (Кількість: " . $quantity . ")<br>";
                                    echo "<hr>";
                                }
                                echo "<p>Сума: " . $row['suma'] . " грн.</p>";
                                ?>
                                <?if($row['stan'] != 1){?>
                                    <a style="float: right; margin: 5px" href="orders.php?func=1&id=<?echo $row['id']?>" class="button">Виконано</a>
                                <?}?>
                                <?if($row['stan'] != 2){?>
                                    <a style="float: right; margin: 5px"  href="orders.php?func=2&id=<?echo $row['id']?>" class="button">Відмінити</a>
                                <?}?>
                                <?if($row['stan'] != 0){?>
                                    <a style="float: right; margin: 5px"  href="orders.php?func=0&id=<?echo $row['id']?>" class="button">Не виконано</a>
                                <?}?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <p>Немає замовлень</p>
            <?php } ?>
        </section>
    </main>
    <script>
        function toggleProducts(rowId) {
            var productsRow = document.getElementById("productsRow_" + rowId);
            productsRow.style.display = productsRow.style.display === "none" ? "table-row" : "none";
        }
    </script>
    </body>
    </html>

<?php
mysqli_close($conn);
?>