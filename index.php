<?php
session_start();
if(!isset($_SESSION['checked'])) {
    header("Location: functions/checkCookie.php");
}
$conn = mysqli_connect("localhost", "root", "", "tv_shop");

if (!$conn) {
die("Connection failed: " . mysqli_connect_error());
}
$sqls = array();
$titles = array();
$sqls[] = "SELECT * FROM products WHERE stock_quantity != 0 ORDER BY selled_quantity DESC LIMIT 5";
$titles[] = "Топ 5 найпопулярніших телевізорів";
$sqls[] = "SELECT * FROM products WHERE manufacturer = 'Samsung' AND stock_quantity != 0 ORDER BY selled_quantity DESC LIMIT 5";
$titles[] = "Топ 5 телевізорів Samsung";
$sqls[] = "SELECT * FROM products WHERE manufacturer = 'LG' AND stock_quantity != 0 ORDER BY selled_quantity DESC LIMIT 5";
$titles[] = "Топ 5 телевізорів LG";
$sqls[] = "SELECT * FROM products WHERE manufacturer = 'Sony' AND stock_quantity != 0 ORDER BY selled_quantity DESC LIMIT 5";
$titles[] = "Топ 5 телевізорів Sony";

?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Головна сторінка</title>
        <link href="css/style.css" rel="stylesheet">
        <link href="css/scrollbar.css" rel="stylesheet">
    </head>
    <body>
    <header>
        <h1>TV Store</h1>
        <nav>
            <ul class="main-nav">
                <li><a href="index.php" class="button">Головна</a></li>
                <li><a href="shop.php" class="button">Магазин</a></li>
                <li><a href="about.php" class="button">Про проект</a></li>
            </ul>
            <ul>
                <?php if (isset($_SESSION['login']) || $_SESSION['login'] != "" ) { ?>
                    <li class="dropdown">
                        <a class="button login"><? echo $_SESSION['login']?></a>
                        <ul class="dropdown-menu">
                            <?php if ($_SESSION['isAdmin']) { ?>
                                <li><a href="admin.php" class="button">Адмінпанель</a></li>
                            <?php }?>
                            <li><a href="cart.php" class="button">Кошик(<?php echo $_SESSION['cart']; ?>)</a></li>
                            <li><a href="logout.php" class="button">Вийти</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li><a href="login.php" class="login">Увійти</a></li>
                    <li><a href="register.php" class="login">Зареєструватись</a></li>
                <?php } ?>
            </ul>
        </nav>
    </header>
    <main>
        <section class="products">
            <? for($i = 0; $i < count($sqls); $i++){
            $result = mysqli_query($conn, $sqls[$i]);
            ?>
            <h2><?echo $titles[$i]?></h2>
                <?php if (mysqli_num_rows($result) == 0) {
                    echo "Пусто.";
                } ?>
                <ul>
                <?php while($row = mysqli_fetch_assoc($result)) { ?>
                    <li class="prod-block">
                        <a href="tv-info.php?id=<?php echo $row['id_product']?>">
                            <img src="<?php echo $row['image']; ?>" alt="<?php echo $row['name']; ?>">
                        </a>
                        <h3><?php echo $row['name']; ?></h3>
                        <p><?php echo $row['screen_size']; ?>"</p>
                        <p>Ціна: <?php echo $row['price']; ?> грн.</p>
                        <a href="functions/add_to_cart.php?add_to_cart=<?php echo $row['id_product']?>" class="button addBut">В корзину</a>
                    </li>
                <?php } ?>
            </ul>
                <?if($i + 1 < count($sqls)){
                    echo "<br><hr><br>";
                }?>
            <?}?>
        </section>
    </main>
    </body>
</html>
<?php
mysqli_close($conn);
?>