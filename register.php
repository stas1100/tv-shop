<?php
$conn = mysqli_connect("localhost", "root", "", "tv_shop");
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$error = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $login = $_POST["login"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $sql = "SELECT * FROM users WHERE login='$login' OR email='$email'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $error = "Користувач з таким логіном або поштою існує!";
    } else {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO users (login, email, password) VALUES ('$login', '$email', '$password')";
        $result = mysqli_query($conn, $sql);
        header("Location: login.php");
    }
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Реєстрація</title>
    <link rel="stylesheet" href="css/reg.css">
</head>
<body>
<h1>Зареєструватись</h1>
<form method="post" action="register.php">
    <p class="error"><?echo $error?></p>
    <label for="username">Логін:</label>
    <input type="text" id="username" name="login" required><br><br>
    <label for="email">Email:</label>
    <input type="email" id="email" name="email" required><br><br>
    <label for="password">Пароль:</label>
    <input type="password" id="password" name="password" required>
    <a href="login.php" class="ref"><p>Уже маєте аккаунт?</p></a>
    <br>
    <input type="submit" value="Зареєструватись">
    <a href="index.php" class="button">На головну</a>
</form>
</body>
</html>
