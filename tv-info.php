<?php
session_start();

$conn = mysqli_connect("localhost", "root", "", "tv_shop");

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$addComm = "empty";
$id = $_GET["id"];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $login = $_SESSION['login'];
    $comment = $_POST['comment'];
    $addComm = "INSERT INTO comments (login, comment, id_product) VALUES ('$login', '$comment', $id)";
    mysqli_query($conn, $addComm);
    header("Location: tv-info.php?id=" . $id);
}

$sql = "SELECT * FROM products WHERE id_product='$id'";
$sqlComm = "SELECT * FROM comments WHERE id_product='$id' ORDER BY created_at DESC";
$getComms = mysqli_query($conn, $sqlComm);
$result = mysqli_query($conn, $sql);
$info = mysqli_fetch_assoc($result);
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?echo $info['name']?></title>
        <link href="css/tv-info.css" rel="stylesheet" >
        <link href="css/scrollbar.css" rel="stylesheet">
    </head>
    <body>
    <div class="header">
        <h1>TV Store</h1>
        <a href="index.php" class="button">На головну</a>
    </div>
    <main>
        <img src="<?echo $info['image']?>" alt="Product Image">
        <div class="left">
        <h2><?echo $info['name']?></h2>
            <p><pre class="desc"><?echo $info['description']?></pre></p>
            <p class="price">Ціна: <?echo $info['price']?></p>
            <form action="functions/add_to_cart.php" method="get">
                <input type="number" id="quantity" name="quantity" value="1" min="1" max="<?echo $info['stock_quantity']?>">
                <button type="submit" name="add_to_cart" value="<?echo $info['id_product']?>">В корзину</button>
            </form>
        </div>
    </main>
    <hr>
    <h1 style="margin: 10px">Коментарі</h1>
    <?if(!empty($_SESSION['login'])){?>
    <button type="submit" id="add-but" onclick="showForm()">Додати коментар</button>
    <div id="comment-form" style="display:none;">
        <form id="comment-form-data" method="post" action="tv-info.php?id=<?echo $id?>">
            <textarea id="comment" name="comment" ></textarea>
            <button type="submit" class="commBut">Додати</button>
        </form>
    </div>
    <hr>
    <?}?>
    <div id="comment-section">
            <?php while($row = mysqli_fetch_assoc($getComms)) { ?>
                <div class='comment'>
                    <?if($_SESSION['isAdmin'] || $row['login'] === $_SESSION['login']){?>
                    <a href="functions/delete_comment.php?id=<?echo $row['id']?>" class="delete">X</a>
                    <?}?>
                    <h4><? echo $row['login']?></h4>
                    <pre><p><? echo $row['comment']?></p></pre>
                    <p style="font-size: 8pt"><? echo $row['created_at']?></p>
                </div>;
            <?php } ?>
    </div>
    <script>
        function showForm(){
            let comm_form = document.getElementById("comment-form");
            let add_but = document.getElementById("add-but");
            comm_form.style.display = "flex";
            add_but.style.display = "none";
        }
    </script>
    </body>
    </html>
<?php
mysqli_close($conn);
?>