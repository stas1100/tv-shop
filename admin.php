<!DOCTYPE html>
<html>
<head>
    <title>Панель адміністратора</title>
    <link rel="stylesheet" href="css/admin.css">
    <link href="css/scrollbar.css" rel="stylesheet">
</head>
<body>

<?php
// Check if user is logged in as admin
session_start();
if (!isset($_SESSION['login']) || !$_SESSION['isAdmin']) {
    header('Location: login.php');
    exit;
}

$conn = mysqli_connect("localhost", "root", "", "tv_shop");

if (isset($_POST['add'])) {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $price = $_POST['price'];
    $screen_size = $_POST['screen_size'];
    $manufacturer = $_POST['manufacturer'];
    $category = $_POST['category'];
    $image = $_POST['image'];
    $quantity = $_POST['quantity'];

    $sql = "INSERT INTO products (name, description, price, screen_size, manufacturer, category, image, stock_quantity)
					VALUES ('$name', '$description', '$price', '$screen_size', '$manufacturer', '$category', '$image', '$quantity')";
    mysqli_query($conn, $sql);
}

if (isset($_POST['delete'])) {
    $id = $_POST['id'];

    $sql = "DELETE FROM products WHERE id_product='$id'";
    mysqli_query($conn, $sql);
}

if (isset($_POST['add-quantity'])) {
    $id = $_POST['id'];
    $quantity = $_POST['quantity'];
    $sql = "UPDATE products SET stock_quantity = stock_quantity + $quantity WHERE id_product =$id";
    mysqli_query($conn, $sql);
}

if (isset($_POST['give-rights'])) {
    $id = $_POST['id'];
    $sql = "UPDATE users SET isAdmin = 1 WHERE id_user =$id";
    mysqli_query($conn, $sql);
}

if (isset($_POST['take-rights'])) {
    $id = $_POST['id'];
    $sql = "UPDATE users SET isAdmin = 0 WHERE id_user =$id";
    mysqli_query($conn, $sql);
    if($id == $_SESSION['user_id']){
        $_SESSION['isAdmin'] = false;
        header('Location: index.php');
    }
}

if (isset($_POST['change-pass'])) {
    $id = $_POST['id'];
    $pass = $_POST['pass'];
    $pass = password_hash($pass, PASSWORD_DEFAULT);
    $sql = "UPDATE users SET password = '{$pass}' WHERE id_user =$id";
    mysqli_query($conn, $sql);
}

mysqli_close($conn);

?>

<div class="header">
    <h1>Панель адміністратора</h1>
    <a href="index.php" class="button">На головну</a>
    <a href="orders.php" class="button">Замовлення</a>
</div>
<div class="container">
    <h2>Додати продукт</h2>
    <form method="POST">
        <label>Назва</label>
        <input type="text" name="name">

        <label>Опис</label>
        <textarea name="description"></textarea>

        <label>Ціна</label>
        <input type="text" name="price">

        <label>Розмір екрану</label>
        <input type="text" name="screen_size">

        <label>Виробник</label>
        <input type="text" name="manufacturer">

        <label>Категорія</label>
        <input type="text" name="category">

        <label>Посилання на картинку</label>
        <input type="text" name="image">

        <label>Кількість</label>
        <input type="text" name="quantity">

        <input type="submit" name="add" value="Додати">
    </form>
</div>


<div class="container">
    <h2>Видалити продукт</h2>
    <form method="POST">
        <label>ID продукту</label>
        <input type="text" name="id">
        <input type="submit" name="delete" value="Видалити">
    </form>
</div>
<div class="container">
    <h2>Додати кількість</h2>
    <form method="POST">
        <label>ID продукту</label>
        <input type="text" name="id">

        <label>Кількість</label>
        <input type="text" name="quantity">

        <input type="submit" name="add-quantity" value="Додати">
    </form>
</div>
<div class="container">
    <h2>Надати права адміністратора</h2>
    <form method="POST">
        <label>ID користувача</label>
        <input type="text" name="id">

        <input type="submit" name="give-rights" value="Надати">
    </form>
</div>
<div class="container">
    <h2>Відібрати права адміністратора</h2>
    <form method="POST">
        <label>ID користувача</label>
        <input type="text" name="id">

        <input type="submit" name="take-rights" value="Відібрати">
    </form>
</div>
<div class="container">
    <h2>Змінити пароль користувача</h2>
    <form method="POST">
        <label>ID користувача</label>
        <input type="text" name="id">

        <label>Новий пароль</label>
        <input type="text" name="pass">

        <input type="submit" name="change-pass" value="Змінити">
    </form>
</div>
</body>
</html>