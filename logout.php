<?php

session_start();
if(empty($_SESSION['login'])){
    header("Location: login.php");
}

unset($_SESSION['login']);
unset($_SESSION['user_id']);
unset($_SESSION['isAdmin']);
unset($_SESSION['cart']);

unset($_COOKIE['user_id']);
setcookie('user_id', '', time() - 3600);

session_destroy();

header("Location: index.php");
exit();
?>