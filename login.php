<?php
$conn = mysqli_connect("localhost", "root", "", "tv_shop");
$error = "";
$login = "";
$cookie_time = 3600;
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $login = $_POST["login"];
    $password = $_POST["password"];
    $sql = "SELECT * FROM users WHERE login='$login'";
    $result = mysqli_query($conn, $sql);
    $info = mysqli_fetch_assoc($result);

    if (mysqli_num_rows($result) == 1 && password_verify($password, $info['password'])) {

        session_start();
        $_SESSION['login'] = $login;
        $_SESSION['user_id'] = $info['id_user'];

        if($info['isAdmin'] == 1){
            $_SESSION['isAdmin'] = true;
        }
        else{
            $_SESSION['isAdmin'] = false;
        }

        $sql2 = "SELECT product_id FROM cart WHERE user_id = '{$_SESSION['user_id']}'";
        $result2 = mysqli_query($conn, $sql2);

        $_SESSION['cart'] = mysqli_num_rows($result2);
        if(!empty($_POST['check'])){
            setcookie('user_id', $_SESSION['user_id'], time()+$cookie_time, '/');
        }

        header("Location: index.php");
        exit();

    } else {
        $error = "Неправильний логін або пароль!";
    }

    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Вхід</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/chbNrb.css">
</head>
<body>
<h1>Увійти</h1>
<form method="post" action="login.php">
    <p class="error"><?echo $error?></p>
    <label for="username">Логін:</label>
    <input type="text" id="username" value="<?echo $login?>" name="login" required><br><br>
    <label for="password">Пароль:</label>
    <input type="password" id="password" name="password" required>
    <input type="checkbox" class="custom-checkbox" id="check" name="check" value="No">
    <label for="check">Запам'ятати мене</label>
    <a href="register.php" class="ref"><p>Не маєте аккаунту?</p></a>
    <br>
    <input type="submit" value="Увійти">
    <a href="index.php" class="button">На головну</a>
</form>
</body>
</html>
